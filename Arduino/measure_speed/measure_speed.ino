
volatile float speed;

void initTimer(void) {

  // Input Capture setup
  // ICNC1: Enable Input Capture Noise Canceler (not used)
  // ICES1: = 1 for trigger on rising edge
  // CS10: = 4 set prescaler to system clock / 64, period = 4 microseconds
  TCCR1A = 0;
  TCCR1B = (0<<ICNC1) | (1<<ICES1) | (3<<CS10);
  TCCR1C = 0;

  // Interrupt setup
  // ICIE1: Input capture
  // TOIE1: Timer1 overflow
  TIFR1 = (1<<ICF1) | (1<<TOV1);        // clear pending
  TIMSK1 = (1<<ICIE1) | (1<<TOIE1);     // and enable

  // Set up the Input Capture pin, ICP1, which corresponds to Arduino D8
  pinMode(8, INPUT_PULLUP);
}

ISR(TIMER1_CAPT_vect) {
  union twobyte {
    uint32_t word;
    uint8_t  byte[2];
  } timevalue;

  TCNT1H = 0; // clear the counter
  TCNT1L = 0; // clear the counter
  timevalue.byte[0] = ICR1L;        // grab captured timer value
  timevalue.byte[1] = ICR1H;        // grab captured timer value
  speed = 468750. / (float)timevalue.word; // Calculate stitches per minute
}

ISR(TIMER1_OVF_vect) {
  speed = 0.;
}

void setup() {
  Serial.begin(115200);
  speed = 0;
  initTimer();
}

void loop() {
Serial.print(speed);
Serial.println(" stitches per minute");
delay(1000);
}
