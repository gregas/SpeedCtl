/*
At each intterupt from a zero-crossing pulse, Timer0 is started
When the timer expeires, the triac trigger is set high and the Timer0 is restarted
When timer zero expires again, the triac trigger is set low and the timer compare interrupt 
is turned off until the next zero crossing 
*/

#define ZERO_CROSS_PIN 3
#define TRIAC_TRIG_PIN 9

volatile float trig_delay = 0xFF; // Initialize to a large value longer than 120 Hz cycle

void trigDelayTimerSetup() { // Timer for triac trigger delay
  TCCR0A = 0;                // Clear Control Reg A
  TCCR0B = 5 <<  CS10;       // CS10; Set prescaler to 1024 (64 us tick)
  TIFR0 = 0;                 // Clear pending timer zero interrupts
}

ISR(TIMER0_COMPA_vect) {
   digitalWrite(TRIAC_TRIG_PIN, HIGH);  // Trigger the triac
 /* //static int state = LOW;
  //Serial.print(state);
  if (state == LOW) {
  
     OCR0A = trig_delay+30;     // Set delay for high level
     state = HIGH;
  } 
  else {
    //digitalWrite(TRIAC_TRIG_PIN, LOW);   // Turn off triac trigger
    TIMSK0 = 0;                          // Turn off compare interrupt until next zero crossing
    state = LOW;
  }
*/
}

void zeroCrossISR() {
  digitalWrite(TRIAC_TRIG_PIN, LOW); 
  OCR0A = trig_delay;  // Set delay
  TCNT0 = 0;          // Clear timer
  TIMSK0 = 1 << OCF0A;  // Enable compare interrupt A 
  //Serial.print('Z');
}

void setup() {
  //Serial.begin(115200);
  trig_delay = 1; // min = 1, max=128
  trigDelayTimerSetup();
  pinMode(TRIAC_TRIG_PIN, OUTPUT);
  digitalWrite(TRIAC_TRIG_PIN, LOW);
  attachInterrupt(digitalPinToInterrupt(ZERO_CROSS_PIN), zeroCrossISR, RISING); // 120 Hz zero-crossing iterrupt 
}

void loop() {

}
