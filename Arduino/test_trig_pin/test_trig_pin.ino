
#define TRIAC_TRIG_PIN 9
int state = 0;

void setup() {
  pinMode(TRIAC_TRIG_PIN, OUTPUT);
  digitalWrite(TRIAC_TRIG_PIN, LOW);
  Serial.begin(115200);
  Serial.print("Setup");
}

void loop() {
  
  Serial.println(state);
  if (state == LOW) {
       digitalWrite(TRIAC_TRIG_PIN, HIGH);
       state = HIGH;
  } 
  else {
    digitalWrite(TRIAC_TRIG_PIN, LOW);
    state = LOW;
  }
  delay(10);
}
