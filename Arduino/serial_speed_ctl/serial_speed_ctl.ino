/*
At each intterupt from a zero-crossing pulse, Timer0 is started
When the timer expeires, the triac trigger is set high and the Timer0 is restarted
When timer zero expires again, the triac trigger is set low and the timer compare interrupt 
is turned off until the next zero crossing 
*/

/* To Do:
 *  - Keep track of time by counting ZC
 *  - On teach ZC transmit: time, command, speed
 *  - Improve variable names
 *  
 *  
 */

#define ZERO_CROSS_PIN 3
#define TRIAC_TRIG_PIN 9

volatile byte trig_delay = 255; // Initialize to a large value longer than 120 Hz cycle
volatile byte zc = 0;
volatile union twobyte {
    uint32_t word;
    uint8_t  byte[2];
  } timevalue;

void trigDelayTimerSetup() { // Timer for triac trigger delay
  TCCR0A = 0;                // Clear Control Reg A
  TCCR0B = 5 <<  CS10;       // CS10; Set prescaler to 1024 (64 us tick)
  TIFR0 = 0;                 // Clear pending timer zero interrupts
}

void speedTimerSetup () {
  // Input Capture setup
  // ICNC1: Enable Input Capture Noise Canceler (not used)
  // ICES1: = 1 for trigger on rising edge
  // CS10: = 4 set prescaler to system clock / 64, period = 4 microseconds
  TCCR1A = 0;
  TCCR1B = (0<<ICNC1) | (1<<ICES1) | (3<<CS10);
  TCCR1C = 0;

  // Interrupt setup
  // ICIE1: Input capture
  // TOIE1: Timer1 overflow
  TIFR1 = (1<<ICF1) | (1<<TOV1);        // clear pending
  TIMSK1 = (1<<ICIE1) | (1<<TOIE1);     // and enable

  // Set up the Input Capture pin, ICP1, which corresponds to Arduino D8
  pinMode(8, INPUT_PULLUP);
}

ISR(TIMER1_CAPT_vect) {
  TCNT1H = 0; // clear the counter
  TCNT1L = 0; // clear the counter
  timevalue.byte[0] = ICR1L;        // grab captured timer value
  timevalue.byte[1] = ICR1H;        // grab captured timer value
  //Serial.print('S');
}

ISR(TIMER1_OVF_vect) {
  timevalue.word = 0xFFFFFFFF; // Set to max value to indicate (near) zero speed
    //Serial.print('O');
}

ISR(TIMER0_COMPA_vect) {
   static int state = LOW;

  if (state == LOW) {
    digitalWrite(TRIAC_TRIG_PIN, HIGH);  // Trigger the triac
    OCR0A = 128;  // Set timeer to trigger again near end of cycle
    state = HIGH;
  }
  else {
    digitalWrite(TRIAC_TRIG_PIN, LOW);
      TIMSK0 = 0;  // Disable compare interrupt A 
    state = LOW; 
  }
  //Serial.print('C');
}

void zeroCrossISR() {
  digitalWrite(TRIAC_TRIG_PIN, LOW); 
  OCR0A = trig_delay;  // Set delay
  TCNT0 = 0;          // Clear timer
  TIMSK0 = 1 << OCF0A;  // Enable compare interrupt A 
  zc = 1;
    //Serial.print('Z');
}

void setup() {
  Serial.begin(115200);
  trigDelayTimerSetup();
  speedTimerSetup();
  pinMode(TRIAC_TRIG_PIN, OUTPUT);
  digitalWrite(TRIAC_TRIG_PIN, LOW);
  attachInterrupt(digitalPinToInterrupt(ZERO_CROSS_PIN), zeroCrossISR, RISING); // 120 Hz zero-crossing iterrupt 
}

void loop() {
  static float seconds = 0;
  float spm;
  int tv;
  byte td;
  if (Serial.available()) {
    // read the most recent byte (which will be from 0 to 255):
    td = Serial.read();
    if (td < 1) td = 1;
    if (td > 128) td = 255; // For a delay longer than 128, turn it off completely
    noInterrupts();
    trig_delay = td;  // min = 1, max=128
    interrupts();
  }
  if (zc == 1) { // only report speed at 60 Hz
    noInterrupts();
    tv = timevalue.word;
    interrupts();
    spm = 468750. / (float)tv; // Calculate stitches per minute
    if (spm < 0) spm = 0;
    seconds += 0.008333333333333333;
    Serial.print(seconds);
    Serial.print(',');
    Serial.print(trig_delay);
    Serial.print(',');
    Serial.println(spm);
    zc = 0;
  }
}
