
volatile float freq;

void initTimer(void) {

  // Input Capture setup
  // ICNC1: Enable Input Capture Noise Canceler (not used)
  // ICES1: = 1 for trigger on rising edge
  // CS10: = 4 set prescaler to system clock / 64, period = 4 microseconds
  TCCR1A = 0;
  TCCR1B = (0<<ICNC1) | (1<<ICES1) | (3<<CS10);
  TCCR1C = 0;

  // Interrupt setup
  // ICIE1: Input capture
  TIFR1 = 1<<ICF1;        // clear pending
  TIMSK1 = 1<<ICIE1;     // and enable

  // Set up the Input Capture pin, ICP1, which corresponds to Arduino D8
  pinMode(8, INPUT);
}

ISR(TIMER1_CAPT_vect) {
  union twobyte {
    uint32_t word;
    uint8_t  byte[2];
  } timevalue;

  TCNT1H = 0; // clear the counter
  TCNT1L = 0; // clear the counter
  timevalue.byte[0] = ICR1L;        // grab captured timer value
  timevalue.byte[1] = ICR1H;        // grab captured timer value
  freq = 1. / (4e-6* (float)timevalue.word); // Calculate frequency (Hz)
}

void setup() {
  Serial.begin(115200);
  freq = 0;
  initTimer();
}

void loop() {
Serial.print(freq);
Serial.println(" Hz");
}
