
volatile int count;

void initTimer(void) {
  // Don't really need to use the timer, but it's expedient because everything is already
  // hooked up to the timer input capture pin.
  TIFR1 = 1<<ICF1;        // clear pending
  TIMSK1 = 1<<ICIE1;     // and enable

  // Set up the Input Capture pin, ICP1, which corresponds to Arduino D8
  pinMode(8, INPUT_PULLUP);
}

ISR(TIMER1_CAPT_vect) {
  count ++;
}

void setup() {
  initTimer();
  Serial.begin(115200);
  count = 0;
}

void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(count);
  delay(100);
}
