EESchema Schematic File Version 2
LIBS:GregsParts
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:speed-ctl-cache
EELAYER 25 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "Sewing Machine Speed Control"
Date ""
Rev "A"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MOC3052M U1
U 1 1 56881D4A
P 2450 2850
F 0 "U1" H 2250 3050 50  0000 L CNN
F 1 "MOC3052M" H 2450 3050 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 2250 2650 50  0001 L CIN
F 3 "" H 2425 2850 50  0000 L CNN
	1    2450 2850
	1    0    0    -1  
$EndComp
$Comp
L ST_T435-800W U2
U 1 1 568826C5
P 4250 3150
F 0 "U2" H 4000 3500 50  0000 C CNN
F 1 "ST_T435-800W" H 3950 2900 50  0000 C CNN
F 2 "Power_Integrations:TO-220" H 4250 3150 50  0001 C CNN
F 3 "" H 4250 3150 50  0000 C CNN
	1    4250 3150
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 56882B04
P 3300 2750
F 0 "R1" V 3380 2750 50  0000 C CNN
F 1 "R" V 3300 2750 50  0000 C CNN
F 2 "Resistors_ThroughHole:Resistor_Horizontal_RM10mm" V 3230 2750 50  0001 C CNN
F 3 "" H 3300 2750 50  0000 C CNN
	1    3300 2750
	0    1    1    0   
$EndComp
Wire Wire Line
	2750 2750 3150 2750
Wire Wire Line
	3450 2750 4250 2750
Wire Wire Line
	2750 2950 3750 2950
Wire Wire Line
	3750 2950 3750 3350
$Comp
L Trinket_Pro_5V U3
U 1 1 568861D3
P 6350 3200
F 0 "U3" H 6350 4000 60  0000 C CNN
F 1 "Trinket_Pro_5V" V 6350 3250 60  0000 C CNN
F 2 "" H 6350 2550 60  0000 C CNN
F 3 "" H 6350 2550 60  0000 C CNN
	1    6350 3200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
