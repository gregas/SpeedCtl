
 import processing.serial.*;
 Serial port;

 void setup() {
 size(128, 150);

 port = new Serial(this, "COM8", 115200);

 // If you know the name of the port used by the Arduino board, you
 // can specify it directly like this.
 //port = new Serial(this, "COM8", 9600);
 }

 void draw() {
 
 // draw a gradient from black to white
 for (int i = 0; i < 128; i++) {
 stroke(i);
 line(i, 0, i, 150);
 }

 // write the current X-position of the mouse to the serial port as
 // a single byte
 port.write(mouseX);
 println(mouseX);
 }
 