# -*- coding: utf-8 -*-
"""
Created on Fri Mar 04 08:07:59 2016

@author: gregas
"""

# To do:
# - Deal with startup sync issues (should line start with a certain char??
import serial
import numpy as np
import matplotlib.pyplot as plt

ser = serial.Serial()
ser.baudrate = 115200
ser.port = 'COM8'
ser.timeout = 1
ser.open()



seconds = 5
d = np.empty((seconds*60, 3), dtype=np.float32)

for i in range(seconds*60):
    try:
        s = ser.readline()
        d[i,:] = [float(x) for x in s.split(',')]
    except:
        ser.close()
        raise

ser.close()
t = d[:,0]
c = d[:,1]
s = d[:,2]
plt.plot(t,c)
plt.plot(t,s)

